import servidorWeb

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p> EL RECURSO EXISTE </p>
    <p> Recurso actual: {html_page}.</p>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p> Recurso no encontrado: {resource} </p>
  </body>
</html>
"""

class contentApp(servidorWeb.webApp):
    contents = {'/': "<p> Pagina principal </p>",
                '/hola': "<p> Hola mundo cruel! </p>",
                '/adios': "<p> Adios mundo cruel! </p>"}

    def parse(self, request):
        requests = request.decode("utf-8") # lo primero que hacemos es decodificar para pasar a string.
        list_requests = requests.split()  # Separamos lo recibido por espacios.
        resource = list_requests[1][0:] # cogemos desde la barra hacia delante.
        print(" El recurso pedido es: " + resource)
        return resource


    def process(self, parsedRequest):
        if parsedRequest in self.contents: # Devolvemos 200 Ok y añadimos la página HTML.
            html_page = self.contents[parsedRequest]
            answer = ("200 Ok", PAGE.format(html_page=html_page)) # creo la respuesta en caso de que el recurso exista.
        else:
            answer = ("404 Not Found", PAGE_NOT_FOUND.format(resource=parsedRequest)) # creo la respuesta en caso de que el recurso no exista.
        return answer

    print(contents)


if __name__ == "__main__":
    app1 = contentApp("localhost", 1234)
    
