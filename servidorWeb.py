#!/usr/bin/env python3

#
# webApp class
# Root for hierarchy of classes implementing web applications
#
# Copyright Jesus M. Gonzalez-Barahona 2009-2020
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
#

import socket

class webApp:
    def parse (self, request):
        print(" NO HACES NADA")
        return None

    def process (self, parsedRequest): # generar página html y código de respuesta 200 Ok, 301, etc.
        print("Process: Returning 200 OK")
        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    def __init__ (self, hostname, port):
        """Initialize the web application."""

        # Create a TCP objet socket and bind it to a port
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(2048)
            print(request)
            parsedRequest = self.parse(request)
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()

if __name__ == "__main__": # en caso de que queramos probar la clase.
    testWebApp = webApp("localhost", 1234)